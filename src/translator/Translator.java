package translator;

import java.util.regex.Pattern;

public class Translator {
	
	protected String sentence;

	public Translator(String sentence) {
		this.sentence = sentence;
	}

	public String getSentence() {
		
		return this.sentence;
	
	}

	public String translate() {
		String result="";
		if(this.sentence.equals(""))
			result = "nil";
			
		//if(Pattern.compile("^[aeoui]",Pattern.CASE_INSENSITIVE).matcher(sentence).find() && sentence.endsWith("y"))
		//if(Pattern.compile("^[aeoui] [a-z]*y$",Pattern.CASE_INSENSITIVE).matcher(sentence).find()) solo lettere alfabeto 
		if(Pattern.compile("^[aeoui]\\w*y$",Pattern.CASE_INSENSITIVE).matcher(sentence).find()) //tutti i caratteri alfanumerici
			result = sentence + "nay";
		
		return result;
		
	}

}
